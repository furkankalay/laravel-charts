<?php

namespace Furkankalay\LaravelCharts\Http\Livewire;


use Livewire\Component;

class Chart extends Component
{

    private $options;

    public function mount($options)
    {
        $this->options = $options;
    }

    public function render()
    {
        return view('laravel-charts::livewire.chart', [
            'options' => $this->options
        ]);
    }
}

