<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class YAxis
{
    public $title;

    public static function create()
    {
        return new YAxis();
    }

    public function title($title)
    {
        $this->title = $title;
        return $this;
    }
}
