<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class ToolTip
{
    public $bar;

    public static function create()
    {
        return new ToolTip();
    }

    public function y($y)
    {
        $this->y = $y;
        return $this;
    }
}
