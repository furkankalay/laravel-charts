<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class XAxis
{
    public $categories = ['12','24','48','100'];

    public static function create()
    {
        return new XAxis();
    }

    public function categories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    public function toString() {
        return json_encode([
            'categories' => $this->categories
        ]);
    }
}
