<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class DataSerie
{
    public  $name = 'default';
    public $data = [100, 200, 300, 400];

    public static function create()
    {
        return new DataSerie();
    }

    public function name($name){
        $this->name = $name;
        return $this;
    }

    public function data($data){
        $this->data = $data;
        return $this;
    }

    public function toString() {


        return json_encode([
            'name' => $this->name,
            'data' => $this->data
        ]);
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
