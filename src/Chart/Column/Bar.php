<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class Bar
{
    public bool $isHorizontal = false;
    public $columnWidth = '55';
    public $endingShape = 'rounded';

    public static function create()
    {
        return new Bar();
    }

    public function isHorizontal($isHorizontal){
        $this->isHorizontal = $isHorizontal;
        return $this;
    }

    public function columnWidth($columnWidth){
        $this->columnWidth = $columnWidth;
        return $this;
    }

    public function endingShape($endingShape){
        $this->endingShape = $endingShape;
        return $this;
    }

    public function toString() {
        return json_encode([
            'horizontal' => $this->isHorizontal,
            'columnWidth' => $this->columnWidth . '%',
            'endingShape' => $this->endingShape,
        ]);
    }


}
