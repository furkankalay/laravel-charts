<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class PlotOptions
{
    public $bar;

    public static function create()
    {
        return new PlotOptions();
    }

    public function bar($bar)
    {
        $this->bar = $bar;
        return $this;
    }
}
