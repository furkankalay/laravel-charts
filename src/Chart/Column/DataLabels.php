<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class DataLabels
{
    public $enabled;

    public static function create()
    {
        return new DataLabels();
    }

    public function isEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }
}
