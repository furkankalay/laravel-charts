<?php

namespace Furkankalay\LaravelCharts\Chart\Column;


class Y
{
    public $bar;

    public static function create()
    {
        return new Y();
    }

    public function formatter($formatter)
    {
        $this->formatter = $formatter;
        return $this;
    }
}
