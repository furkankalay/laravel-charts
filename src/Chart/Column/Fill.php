<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class Fill
{

    public $opacity;

    public static function create()
    {
        return new Fill();
    }

    public function opacity($opacity)
    {
        $this->opacity = $opacity;
        return $this;
    }
}
