<?php

namespace Furkankalay\LaravelCharts\Chart\Column;

class Stroke
{

    public bool $show = true;
    public int $width = 2;
    public string $colors = 'transparent';

    public static function create()
    {
        return new Stroke();
    }

    public function show($show){
        $this->show = $show;
        return $this;
    }

    public function width($width){
        $this->width = $width;
        return $this;
    }

    public function colors($colors){
        $this->colors = $colors;
        return $this;
    }

    public function StrokeToString(){

        return json_encode([
            'show' => $this->show,
            'width' => $this->width,
        'colors' => [$this->colors],

        ]);
    }
}
