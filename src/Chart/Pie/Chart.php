<?php

namespace Furkankalay\LaravelCharts\Chart\Pie;

class Chart
{

    public $responsiveWidth;
    public $responsiveHeight;

    public static function create()
    {
        return new Chart();
    }

    public function width($responsiveWidth)
    {
        $this->responsiveWidth = $responsiveWidth;
        return $this;
    }

    public function height($responsiveHeight)
    {
        $this->responsiveHeight = $responsiveHeight;
        return $this;
    }
}
