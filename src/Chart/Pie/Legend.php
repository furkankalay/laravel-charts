<?php

namespace Furkankalay\LaravelCharts\Chart\Pie;

class Legend
{

    public $responsivePosition;

    public static function create()
    {
        return new Legend();
    }

    public function responsivePosition($responsivePosition)
    {
        $this->responsivePosition = $responsivePosition;
        return $this;
    }
}
