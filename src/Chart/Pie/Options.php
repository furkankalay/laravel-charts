<?php

namespace Furkankalay\LaravelCharts\Chart\Pie;

class Options
{
    public $chart;

    public $legend;

    public static function create()
    {
        return new Options();
    }

    public function chart($chart)
    {
        $this->chart = $chart;
        return $this;
    }

    public function legend($legend)
    {
        $this->legend = $legend;
        return $this;
    }
}
