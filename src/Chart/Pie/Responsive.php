<?php

namespace Furkankalay\LaravelCharts\Chart\Pie;

class Responsive
{

    public $breakpoint;

    public $options;

    public static function create()
    {
        return new Responsive();
    }

    public function breakpoint($breakpoint)
    {
        $this->breakpoint = $breakpoint;
        return $this;
    }

    public function options($options)
    {
        $this->options = $options;
        return $this;
    }
}
