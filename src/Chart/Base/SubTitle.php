<?php

namespace Furkankalay\LaravelCharts\Chart\Base;

class SubTitle
{

    public string $text = 'default';
    public $align;

    public static function create()
    {
        return new SubTitle();
    }

    public function text($text)
    {
        $this->text = $text;
        return $this;
    }

    public function align($align)
    {
        $this->align = $align;
        return $this;
    }
}
