<?php

namespace Furkankalay\LaravelCharts\Chart;

class ChartOptions extends BaseChartOptions
{
    public $pieSeries;

    public $stroke;
    public array $labels;
    public string $chartId;
    public $xAxis;
    public $yAxis;
    public  $plotOptions;
    public  $toolTip;
    public  $curve;
    public bool $stacked = false;

    public static function create()
    {
        return new ChartOptions();
    }

    public function labels($labels)
    {
        $this->labels = $labels;
        return $this;
    }

    public function curve($curve){
        $this->curve = $curve;
        return $this;
    }

    public function stroke($stroke){
        $this->stroke = $stroke;
        return $this;
    }

    public function plotOptions($plotOptions){
        $this->plotOptions = $plotOptions;
        return $this;
    }

    public function yAxis($yAxis){
        $this->yAxis = $yAxis;
        return $this;
    }

    public function toolTip($toolTip){
        $this->toolTip = $toolTip;
        return $this;
    }

    public function xAxis($xAxis){
        $this->xAxis = $xAxis;
        return $this;
    }

    public function stacked($stacked)
    {
        $this->stacked = $stacked;
        return $this;
    }

    public function series($pieSeries)
    {
        $this->pieSeries = $pieSeries;
        $this->pieSeries = implode(',', $this->pieSeries);
        return $this;
    }



    public function colorsToString() {
        return json_encode([
            $this->colors
        ]);
    }
}
