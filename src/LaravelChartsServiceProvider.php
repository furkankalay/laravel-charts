<?php

namespace Furkankalay\LaravelCharts;



use Furkankalay\LaravelCharts\Http\Livewire\Chart;
use Furkankalay\LaravelCharts\Http\Livewire\LineChart;
use Furkankalay\LaravelCharts\Http\Livewire\PieChart;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Livewire\Livewire;

class LaravelChartsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->afterResolving(BladeCompiler::class, function () {
            Livewire::component('laravel-charts::chart', Chart::class);
//            Livewire::component('laravel-charts::pie-chart', PieChart::class);
        });


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravel-charts');
    }
}
