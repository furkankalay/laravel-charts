<div>
    <div class="" id="{{$options->chartId}}"></div>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <script>

        var options = {
            series: [
                @if($options->multiSeries)
                    @foreach($options->multiSeries as $serie)
                    {!! $serie->toString() !!},
                    @endforeach

                    @endif

                    @isset($options->pieSeries)
                [{{$options->pieSeries}}]
                @endisset
            ],
            labels: [
                @isset($options->pieSeries)
                    @foreach($options->labels as $label)
                    '{{$label}}',
                @endforeach
                @endisset
            ],
            @if($options->colors)
            colors: [
                @foreach($options->colors as $color)
                    '{!! $color !!}',
                @endforeach
            ],
            @endif
            chart: {
                type: '{{$options->type ?? 'bar'}}',
                @if($options->height)
                height: {{$options->height}},

                @endif

                    @if($options->width)
                width: {{$options->width}},

                @endif

                    @if($options->stacked)
                stacked: {{$options->stacked}},

                @endif


                zoom: {
                    enabled: {{$options->zoom ?? 'false'}}
                }

            },
            plotOptions: {
                {{--    {{ $options->bar->color ?? 'FFFFFF' }}--}}
                    @if($options->plotOptions)
                bar: {!! $options->plotOptions->bar->toString()  !!},

                @endif

            },
            dataLabels: {
                enabled: false,
            },

            @if($options->stroke)
            stroke:


                {!! $options->stroke->strokeToString() !!},

            @if($options->curve)

            curve: '{{$options->curve}}'
            @endif
            ,
            @endif
            stroke: {
                width: 5
            },
            title: {

                @if($options->title?->text)
                text: '{{$options->title?->text}}',
                @endif
                    @if($options->title?->align)
                align: '{{$options->title?->align}}'
                @endif

            },
            subtitle: {

                @if($options->subTitle?->text)
                text: '{{$options->subTitle?->text}}',
                @endif

                    @if($options->subTitle?->align)
                align: '{{$options->subTitle?->align}}'
                @endif

            },
            @if($options->xAxis)
            xaxis:
                {!! $options->xAxis->toString()  !!},
            @endif
            yaxis: {
                title: {

                    text:
                        '{{$options->yAxis->title->text ?? 'default'}}',

                }
            },
            // grid: {
            //     row: {
            //         colors: ['#f3f3f3', 'transparent'],
            //         opacity: 0.5
            //     },
            // },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {

                    formatter: function (val) {
                        return '{{$options->toolTip->y->formatter ?? 'default {value} default'}}'.replace('{value}', val)
                    }

                }
            },
            @if($options->responsive)

            responsive: [
                    @foreach($options->responsive as $respo)
                {

                    breakpoint: {{ $respo->breakpoint}},

                    options: {
                        chart: {


                            @if($respo->options?->chart?->responsiveWidth)
                            width: {{$respo->options->chart->responsiveWidth}},
                            @endif

                                @if($respo->options?->chart?->responsiveHeight)
                            height: {{$respo->options->chart->responsiveHeight}}
                                @endif

                        },
                        @if($respo->options?->legend?->responsivePosition)
                        legend: {
                            position: '{{$respo->options->legend->responsivePosition}}'
                        }
                        @endif
                    }

                },
                @endforeach
            ]
            @endif
        };


        var chart = new ApexCharts(document.querySelector("#{{$options->chartId}}"), options);
        chart.render();

        document.addEventListener('DOMContentLoaded', () => {
            Livewire.on('updatedChartData', (chartId, data) => {
                if (chartId == '{{$options->chartId}}') {
                    console.log(data)
                    chart.updateSeries(data)
                    options.series = data
                }
            })

            Livewire.on('updatedChartOptions', (chartId, data) => {
                chart.destroy();
                options.chart.type = data
                chart = new ApexCharts(document.querySelector("#{{$options->chartId}}"), options);
                chart.render();
            })
        });

    </script>
</div>
